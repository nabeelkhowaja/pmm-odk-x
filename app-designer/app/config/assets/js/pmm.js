'use strict';
/* global $, odkTables */
/* exported display */

/**
 * Responsible for rendering the home screen.
 */
function display() {
	
    var body = $('#main');
    // Set the background to be a picture.
    body.css('background-image', 'url(img/analysis.jpeg)');

    var viewTeaTypesButton = $('#view-tfa');
    viewTeaTypesButton.on(
        'click',
        function() {
            odkTables.openTableToListView(
				null,
                'troubleshooting_failure_anaylsis',
                null,
                null,
                'config/tables/troubleshooting_failure_anaylsis/html/troubleshooting_failure_anaylsis_list.html');
        }
    );

    var viewTeaTypesButton = $('#view-followup');
    viewTeaTypesButton.on(
        'click',
        function() {
            odkTables.openTableToListView(
				null,
                'follow_up',
                null,
                null,
                'config/tables/follow_up/html/follow_up_list.html');
        }
    );
}
