'use strict';

var tfaResultSet = {};

/**
 * Assumes tfaResultSet has valid content
 *
 * Updates document field with the value for the elementKey
 */
function nullCaseHelper(elementKey, documentSelector) {
  var temp = tfaResultSet.get(elementKey);
  if (temp !== null && temp !== undefined) {
    $(documentSelector).text(temp);
  }else{
    $(documentSelector).text("<unknown>");
  }
}

/**
 * Assumes tfaResultSet has valid content.
 *
 * Updates the document content with the information from the tfaResultSet
 */
function updateTfaContent() {

  nullCaseHelper('brand_and_model', '#brand_and_model');
  nullCaseHelper('refrigerator_id', "#refrigerator_id");
  nullCaseHelper('health_facility', '#health_facility');

  nullCaseHelper('component_failure', '#component_failure');
  nullCaseHelper('failure_cause_1', "#failure_cause_1");
  nullCaseHelper('failure_cause_2', '#failure_cause_2');

  nullCaseHelper('tfa_visit_needed', '#tfa_visit_needed');
  nullCaseHelper('repairs_concerns_technician', "#repairs_concerns_technician");
  nullCaseHelper('replacement_parts_needed', '#replacement_parts_needed');

  nullCaseHelper('refrigerator_place', '#refrigerator_place');
  nullCaseHelper('temperature_alarm_caused', '#temperature_alarm_caused');
  nullCaseHelper('refrigerator_cleaned', '#refrigerator_cleaned');
  nullCaseHelper('refrigerator_drained', '#refrigerator_drained');
  nullCaseHelper('refrigerator_exposed', '#refrigerator_exposed');
  nullCaseHelper('refrigerator_ventilation_space', '#refrigerator_ventilation_space');
  nullCaseHelper('reading_temperature_display', '#reading_temperature_display');
  nullCaseHelper('water_collected_refrigerator', '#water_collected_refrigerator');
  nullCaseHelper('water_height', '#water_height');
  nullCaseHelper('store_vaccines', '#store_vaccines');
  nullCaseHelper('gasket_damage', '#gasket_damage');
  nullCaseHelper('gasket_observation', '#gasket_observation');
  nullCaseHelper('corrosion_refrigerator', '#corrosion_refrigerator');
  nullCaseHelper('corrosion_obervation', '#corrosion_obervation');
  nullCaseHelper('condensation_drain_problems', '#condensation_drain_problems');
  nullCaseHelper('condensation_drain_observation ', '#condensation_drain_observation ');
  nullCaseHelper('power_cable_damage', '#power_cable_damage');
  nullCaseHelper('power_cable_damage_observation', '#power_cable_damage_observation');
  nullCaseHelper('refrigerator_noise_smell', '#refrigerator_noise_smell');
  nullCaseHelper('refrigerator_noise_smell_observation', '#refrigerator_noise_smell_observation');
  nullCaseHelper('solar_array_location', '#solar_array_location');
  nullCaseHelper('repair_solar_system', '#repair_solar_system');
  nullCaseHelper('repair_why_what_actions', '#repair_why_what_actions');
  nullCaseHelper('solar_panels_cleaned', '#solar_panels_cleaned');

  nullCaseHelper('main_component_first_reported', '#main_component_first_reported');
  nullCaseHelper('main_component_second_reported', '#main_component_second_reported');
  nullCaseHelper('main_component_third_reported', '#main_component_third_reported');
  nullCaseHelper('door_hinges_loose', '#door_hinges_loose');
  nullCaseHelper('door_alignment_gap', '#door_alignment_gap');
  nullCaseHelper('access_cover_tempering', '#access_cover_tempering');
  nullCaseHelper('power_led_on_refrigerator', '#power_led_on_refrigerator');
  nullCaseHelper('ice_liner_intact', '#ice_liner_intact');
  nullCaseHelper('thermostat_setpoint', '#thermostat_setpoint');
  nullCaseHelper('signs_problem_electronic_compartment', '#signs_problem_electronic_compartment');
  nullCaseHelper('signs_problem_electronic_compartment_detail', '#signs_problem_electronic_compartment_detail');
  nullCaseHelper('leaks_electronic_compartment', '#leaks_electronic_compartment');
  nullCaseHelper('leaks_electronic_compartment_detail', '#leaks_electronic_compartment_detail');
  nullCaseHelper('fan_working_electronic_compartment', '#fan_working_electronic_compartment');
  nullCaseHelper('illicit_repairs_electronic_compartment', '#illicit_repairs_electronic_compartment');
  nullCaseHelper('illicit_repairs_electronic_compartment_detail', '#illicit_repairs_electronic_compartment_detail');
  nullCaseHelper('operating_range_voltage_stabilizer', '#operating_range_voltage_stabilizer');
  nullCaseHelper('input_voltage_reading', '#input_voltage_reading');
  nullCaseHelper('output_voltage_reading', '#output_voltage_reading');
  nullCaseHelper('replaced_model_same_device', '#replaced_model_same_device');
  nullCaseHelper('replaced_model_new_device', '#replaced_model_new_device');
  nullCaseHelper('replaced_provided_or_purchased', '#replaced_provided_or_purchased');
  nullCaseHelper('physical_damage_voltage_stabilizer', '#physical_damage_voltage_stabilizer');
  nullCaseHelper('problems_connections_voltage_stabilizer', '#problems_connections_voltage_stabilizer');
  nullCaseHelper('voltage_stabilizer_available_use', '#voltage_stabilizer_available_use');
  nullCaseHelper('solar_system_grounded', '#solar_system_grounded');

}

function cbSuccess(result) {
  tfaResultSet = result;
  updateTfaContent();
}

function cbFailure(error) {
  console.log('troubleshooting_failure_analysis getViewData CB error : ' + error);
}

var display = function() {
  odkData.getViewData(cbSuccess, cbFailure);
};
