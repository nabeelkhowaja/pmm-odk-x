'use strict';

var tfaResultSet = {};

/**
 * Assumes tfaResultSet has valid content
 *
 * Updates document field with the value for the elementKey
 */
function nullCaseHelper(elementKey, documentSelector) {
  var temp = tfaResultSet.get(elementKey);
  if (temp !== null && temp !== undefined) {
    $(documentSelector).text(temp);
  }else{
    $(documentSelector).text("<unknown>");
  }
}

/**
 * Assumes tfaResultSet has valid content.
 *
 * Updates the document content with the information from the tfaResultSet
 */
function updateTfaContent() {

  nullCaseHelper('brand_and_model', '#brand_and_model');
  nullCaseHelper('refrigerator_id', "#refrigerator_id");
  nullCaseHelper('health_facility', '#health_facility');

  nullCaseHelper('component_failure', '#component_failure');
  nullCaseHelper('failure_cause_1', "#failure_cause_1");
  nullCaseHelper('failure_cause_2', '#failure_cause_2');

  nullCaseHelper('component_non_failure', '#component_non_failure');
  nullCaseHelper('concern', "#concern");
  nullCaseHelper('observations', '#observations');

  nullCaseHelper('ac_voltage', '#ac_voltage');
  nullCaseHelper('ac_frequency', '#ac_frequency');
  nullCaseHelper('appliance_ac_voltage', '#appliance_ac_voltage');
  nullCaseHelper('appliance_ac_frequency', '#appliance_ac_frequency');
  nullCaseHelper('led_diagnostic_result', '#led_diagnostic_result');
  nullCaseHelper('stabilizer_ac_voltage', '#stabilizer_ac_voltage');
  nullCaseHelper('power_outlet_ac_voltage', '#power_outlet_ac_voltage');
  nullCaseHelper('dc_voltage', '#dc_voltage');
  nullCaseHelper('appliance_dc_voltage', '#appliance_dc_voltage');
  nullCaseHelper('open_circuit_voltage', '#open_circuit_voltage');
  nullCaseHelper('irradiance_solar_array', '#irradiance_solar_array');
  nullCaseHelper('severity_solar_panels', '#severity_solar_panels');
  nullCaseHelper('current_orientation_solar_array', '#current_orientation_solar_array');
  nullCaseHelper('optimal_orientation_solar_array', '#optimal_orientation_solar_array');
  nullCaseHelper('current_tilt_solar_array', '#current_tilt_solar_array');
  nullCaseHelper('optimal_tilt_solar_array', '#optimal_tilt_solar_array');

  nullCaseHelper('thermocouple_reading_location', '#thermocouple_reading_location');
  nullCaseHelper('thermostat_setpoint', '#thermostat_setpoint');
  nullCaseHelper('thermostat_sensor_location', '#thermostat_sensor_location');
  nullCaseHelper('thermostat_sensor_exposure', '#thermostat_sensor_exposure');
  nullCaseHelper('thermostat_sensor_airflow', '#thermostat_sensor_airflow');
  nullCaseHelper('thermocouple_reading_external', '#thermocouple_reading_external');
  nullCaseHelper('external_display_temperature', '#external_display_temperature');
  nullCaseHelper('thermocouple_reading_compartment', '#thermocouple_reading_compartment');
  
  nullCaseHelper('fan_issues_observed', '#fan_issues_observed');
  nullCaseHelper('compressor_discharge_pipe_temperature', '#compressor_discharge_pipe_temperature');

  nullCaseHelper('gaps_air_escape', '#gaps_air_escape');
  nullCaseHelper('gasket_damage', '#gasket_damage');
  nullCaseHelper('gap_offset', '#gap_offset');
  nullCaseHelper('hinge_degradation', '#hinge_degradation');

  nullCaseHelper('thermal_storage_condition', '#thermal_storage_condition');
  nullCaseHelper('thermal_storage_issue', '#thermal_storage_issue');

  nullCaseHelper('condensation_depth', '#condensation_depth');

  nullCaseHelper('ambient_temperature', '#ambient_temperature');
  nullCaseHelper('relative_humidity', '#relative_humidity');

}

function cbSuccess(result) {
  tfaResultSet = result;
  updateTfaContent();
}

function cbFailure(error) {
  console.log('troubleshooting_failure_analysis getViewData CB error : ' + error);
}

var display = function() {
  odkData.getViewData(cbSuccess, cbFailure);
};
